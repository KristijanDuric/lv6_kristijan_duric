﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            
                Notebook notebook = new Notebook();
                notebook.AddNote(new Note("My Note", "Text text text "));
                notebook.AddNote(new Note("My Note_2", "Secon Text second text"));
                notebook.AddNote(new Note("Long long Note Title", "Short Note"));

                Iterator iterator = new Iterator(notebook);
                iterator.Current.Show();
                iterator.Next().Show();

                do
                {
                    iterator.Current.Show();
                    iterator.Next();
                } while (iterator.IsDone != true);
        }
    }
}
