﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    interface IAbstractIterator
    {
        Note First();
        Note Next();
        bool IsDone { get; }
        Note Current { get; }
    }
}
