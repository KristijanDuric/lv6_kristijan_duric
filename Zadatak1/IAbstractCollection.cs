﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
